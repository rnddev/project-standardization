# API V1

This documentation would explain how to interact with backend, 
including the routes, sign (headers), parameters, and response. 
You could see all interaction in the list below.

## Quick Notes

Recommended to install `Markdown All in One` by Yu Zhang in your VS Code.
It would help you formatting the table.

The base host and routes to use the apis are
```bash
# Format
<version>.api.<minor>.<major>.<app-name>.domain/api/<version>/<major>/<minor>/<dataset>/:q1/<dataset>/:q2/
api.<major>.<app-name>.domain/api/<version>/<minor>/<dataset>/:q1/<dataset>/:q2/

# Example
https://api.example.com/v1/
https://localhost:8000/api/v1/
https://v1.api.humas.eksternal.oms.meac.id/api/v1/eksternal/humas/sponsor/1/kompensasi
https://api.eksternal.oms.meac.id/api/v1/humas/sponsor/1/kompensasi
```

**Base Response**
```bash
HTTP/1.1 200 Ok
Content-Type: text/json

{
    "code":200,
    "status":"ok"
}
```

**Message Response**
```bash
HTTP/1.1 200 Ok
Content-Type: text/json

{
    "code":200,
    "status":"ok",
    "message":"auth/logged-in",
    "description":"User credential is accepted."
}
```

**Data Response**
```bash
HTTP/1.1 200 Ok
Content-Type: text/json

{
    "code":200,
    "status":"ok",
    "message":"auth/logged-in",
    "description":"User credential is accepted.",
    "data":{}
}
```

**Pagination Response Body**
```bash
HTTP/1.1 200 Ok
Content-Type: text/json

{
    "code":200,
    "status":"ok",
    "message":"auth/logged-in",
    "description":"User credential is accepted.",
    "page":{
        "current_page": 1,
        "per_page": 20,
        "total": 240,
        "total_page": 12,
        "original": {
            ... in case pagination has been parsing DB or Collection ...
        }
    },
    "data":[]
}
```

**Error Response**
```bash
HTTP/1.1 400 Bad Request
Content-Type: text/json

{
    "code":400,
    "status":"bad request",
    "message":"auth/registration-invalid",
    "description":"The form is not filled with correct format.",
    "error":{
        "password": "Password must atleast 6 characters, has 1 lowercase, 1 uppercase, and 1 symbol."
    }
}
```

`message` attribute is using message object. It follow simple format as `<major-feature>/<minor-feature>/<condition>` in lowercase and kebab style. The purpose while using this format is to make frontend easier to identify current process' condition.

`page.current_page` is counted from 1.

`data` can be array and `error` show which field is not fully meet format or validation failed.

## Table of Contents

- `GET  /api/v1/users`
- `POST /api/v1/users/{userid}`

## `GET  /api/v1/users`

Request: `GET /api/v1/users`

Headers:

*All headers are configured automatically*

*Need authenticated user to use this feature*

Query:

| Key                        | Value                         | Description             |
| -------------------------- | ----------------------------- | ----------------------- |
| `search` (optional)        | (string)                      | No description provided |
| `sort_by` (optional)       | `poins` default: `poins`      | No description provided |
| `sort_type` (optional)     | `asc`, `desc` default: `desc` | No description provided |
| `items_perpage` (optional) | (integer) default: `10`       | No description provided |
| `page` (optional)          | (integer) default: `1`        | No description provided |

Response:

```
HTTP/1.1 200 OK
Content-Type: text/json

[
    {
        "fullname":"Full Name",
        "username":"username123",
        "poins": 200,
        "rank": 4
    },
    ...
]
```

```
HTTP/1.1 401 Unauthorized
Content-Type: text/json

{
    "code":401,
    "status":"unauthorized",
    "error":"auth/no-credential"
}
```

## `POST /api/v1/users`

Request: `POST /api/v1/users`

Headers:

*Need authorized admin to use this feature*

Query:

| Key     | Value                              | Description             |
| ------- | ---------------------------------- | ----------------------- |
| `type`  | `add_poin`, `use_coin`, `add_card` | No description provided |
| `value` | (integer)                          | No description provided |

Response :

**`type` = `add_poin`**

> nothing to see here

**`type` = `use_coin`**

```
HTTP/1.1 200 OK
Content-Type: text/json

{
    "code":200,
    "status":"ok",
    "message":"user/coins-spent",
    "data":{
        "before": ...coins...,
        "current": ...spent coins...,
        "coin_spent": ... `value` from query ...
    }
}
```

```
HTTP/1.1 400 Bad Request
Content-Type: text/json

{
    "code":200,
    "status":"ok",
    "message":"user/coins-insufficient",
    "data":{
        "current": ...coins...,
        "after_spent": ...spent coins...,
        "coin_would_spent": ... `value` from query ...
    }
}
```

**Default Errors**
```
HTTP/1.1 400 Bad Request
Content-Type: text/json

{
    "code":400,
    "status":"bad request",
    "message":"user/type-invalid"
}
```

```
HTTP/1.1 401 Unauthorized
Content-Type: text/json

{
    "code":401,
    "status":"unauthorized",
    "error":"auth/no-credential"
}
```
