# Nama Framework
Berikan latar belakang mengapa framework ini perlu distandarisasi mengikuti kebutuhan dan budaya organisasi.

Diharapkan hanya memasukkan panduan yang dapat diterapkan secara global 
di dalam repository ini. Jika ada panduan lain secara spesifik seperti 
Frontend_Standarization, ditaruh dalam repository lain dengan meng-cloning 
repository ini. Kemudian cukup hapus file-file yang tidak dibutuhkan dan 
menyesuaikan dokumentasi sesuai kebutuhan.

## Persiapan

Dalam repository ini terdapat beberapa file:
- [Dokumentasi Framework - file ini](README.md)
- [Dokumentasi Project](Project-README.md)
- [Template APIs](docs/APIS.md)
- [Template Dokumentasi Fitur](docs/struktur-databases-seeder.md)
- [Standarisasi pengerjaan dengan Git](docs/standarisasi-git.md)

## Modul yang Digunakan

## Instalasi

## Konfigurasi

Untuk mempermudah dalam membuat dokumentasi ini, terdapat beberapa 
ekstensi yang dapat digunakan, yaitu:
- [Markdown All in One by yzhang](https://open-vsx.gitpod.io/vscode/item?itemName=yzhang.markdown-all-in-one)
- [Markdown Editor by zaaack](https://open-vsx.gitpod.io/vscode/item?itemName=zaaack.markdown-editor)

### Konfigurasi Build Tools

### Konfigurasi Program
## Menjalankan Program

## Konsep

Dalam repository ini juga terdapat beberapa panduan :
- Semantic Commit, Branching, and Merging.

## Struktur Proyek

- Config
- Databases
    - Migration
    - [Seeder](docs/struktur-databases-seeder.md)
    - Factory
- Routes
- Controller

## Lainnya

## Catatan

Disarankan menginstal `Markdown All in One` oleh Yu Zhang untuk mempermudah 
penulisan markdown dan juga mendesain tabel.

Jika diperlukan, juga bisa menginstal `Markdown Editor` oleh zaaack untuk
menulis markdown dengan cara WYSIWYG.

Standarisasi framework/proyek ini disarankan menggunakan bahasa Indonesia, 
sedangkan penulisan dokumentasi proyek dengan bahasa Inggris. Hal ini disarankan 
sebab proyek ada kemungkinan untuk di publikasi sehingga bisa digunakan secara 
internasional.

## Referensi
- Semantic API Versioning : https://scotch.io/bar-talk/designing-a-restful-web-api
- Semantic Versioning : https://semver.org/